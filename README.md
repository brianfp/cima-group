## How to install

1. Run `composer install`
2. Run `yarn install`
3. Run `php artisan migrate --seed`
4. Run `php artisan passport:install`

## Credentials

    User: bflorian@cimagroup.test
    Password: 12345678

## Components

- Vue3: https://vuejs.org/guide/introduction.html
- Vuetify: https://next.vuetifyjs.com/en/getting-started/installation/

