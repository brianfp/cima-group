<?php

namespace App\Providers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;
use Illuminate\Pagination\LengthAwarePaginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('success', function ($data, string $message = null, int $status = 200) : JsonResponse {
            return Response::json([
                'status' => 'OK',
                'message' => $message,
                'data'    => $data,
            ], $status);
        });

        Response::macro('successDataTable', function (LengthAwarePaginator $data, array $tableHeaders) : JsonResponse {
            $data = collect($data->toArray())->merge([
                'headers' => collect($tableHeaders)->map(function ($value, $key) {
                    return [
                        'title' => $value,
                        'name' => $key
                    ];
                })->values()
            ]);
            return Response::json($data, 206);
        });

        Response::macro('error', function ($message, array $data = null, int $status = 400) : JsonResponse {
            return Response::json([
                'status' => 'error',
                'message' => $message,
                'data' => $data
            ], $status);
        });
    }
}
