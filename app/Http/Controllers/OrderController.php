<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order = Order::with('products')->paginate(15);
        return Response::successDataTable($order, [
            'id' => 'ID',
            'supplier' => 'Proveedor',
            'type' => 'Tipo',
            'quantity' => 'Cantidad',
            'sale_total' => 'Venta Total'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = Order::create([
            'supplier' => $request->supplier,
            'type' => $request->type
        ]);

        $syncData = [];
        $prices = $request->prices;
        $quantities = $request->quantities;
        foreach($request->product_ids as $key => $product) {
            $productModel = Product::find($product);
            $syncData[$product] = [
                'quantity' => $quantities[$key],
                'price' => $prices[$key]
            ];

            $productModel->selling_price = $prices[$key];
            if ($request->type == 'compra'){
                $productModel->sku += $quantities[$key];
            } else {
                $productModel->sku -= $quantities[$key];
            }
            $productModel->save();
        }

        $order->products()->sync($syncData);
        return Response::success([], 'Orden creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
