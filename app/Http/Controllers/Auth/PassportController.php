<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Laravel\Passport\Client;
use App\Http\Controllers\Controller;
use App\Http\Requests\Passport\PassportLoginRequest;
use Illuminate\Support\Facades\{Auth, Http, Response};

class PassportController extends Controller
{
    /**
     * Api de login
     *
     * @param PassportLoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(PassportLoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return Response::error('Datos no validos');
        }

        if (!Auth::attempt(['email' => $user->email, 'password' => $request->password])) {
            return Response::error('Datos no validos');
        }

        $oauth = Client::where('id', 2)->first();
        $response = Http::post(route('passport.token'), [
            'grant_type' => 'password',
            'client_id' => 2,
            'client_secret' => $oauth->secret,
            'username' => $user->email,
            'password' => $request->password,
            'scope' => ''
        ]);

        return Response::success([
            'user' => $user,
            'tokens' => [
                'auth' => $response->json()
            ]
        ], 'User login successfully.');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(): \Illuminate\Http\JsonResponse
    {
        auth()->user()->token()->revoke();
        return Response::success(null, 'Se cerro sesión correctamente');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function isAuth(): \Illuminate\Http\JsonResponse
    {
        return Response::success(auth()->user() != null);
    }
}
