<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\Auth\PassportController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [PassportController::class, 'login']);
Route::post('/is_auth', [PassportController::class, 'isAuth'])->name('isAuth');

Route::middleware('auth:api')->group(function () {
    Route::post('/logout', [PassportController::class, 'logout'])->name('logout');
    Route::prefix('admin')->name('admin.')->group(function () {
        Route::apiResource('/users', UserController::class)->except(['create', 'edit']);
        Route::apiResource('/products', ProductController::class)->except(['create', 'edit']);
        Route::apiResource('/orders', OrderController::class)->except(['create', 'edit']);
    });
});

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
