import './bootstrap';
import { createApp } from 'Vue';
import Routes from './routes';
import App from './views/App';
import store from './store';

// Vuetify
import 'vuetify/styles';
import { createVuetify } from 'vuetify';
import * as components from 'vuetify/components';
import * as directives from 'vuetify/directives';
import '@mdi/font/css/materialdesignicons.css';
import colors from 'vuetify/lib/util/colors';

// SweetAlert
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

Routes.beforeEach((to, from, next) => {
  if (to.name == 'login' && store.getters.token != null) {
    next({name: 'user.index'});
  }

  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.loggedIn) {
      next({name: 'login'})
    } else {
      axios.defaults.headers.common['Authorization'] = `Bearer ${store.getters.token}`
      next()
    }
  }
  next()
});

const vuetify = createVuetify({
    components,
    directives,
    theme: {
        themes: {
          light: {
            dark: false,
            colors: {
              primary: colors.blue.darken1, // #E53935
              secondary: colors.blue.lighten4, // #FFCDD2
            }
          },
        },
    }
});

createApp(App)
    .use(Routes)
    .use(vuetify)
    .use(store)
    .use(VueSweetalert2)
    .mount('#app');