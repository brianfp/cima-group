import Admin from '../layouts/Auth'

// Views
import User from '../views/users/Index'
import UserEdit from '../views/users/Edit'
import Product from '../views/products/Index'
import ProductEdit from '../views/products/Edit'
import Order from '../views/orders/Index'


export default [
    {
        path: '/admin',
        component: Admin,
        children: [
            {
                name: 'user.index',
                path: '/admin/users',
                component: User
            },
            {
                name: 'user.edit',
                path: '/admin/users/:id',
                component: UserEdit
            },
            {
                name: 'product.index',
                path: '/admin/products',
                component: Product
            },
            {
                name: 'product.edit',
                path: '/admin/products/:id',
                component: ProductEdit
            },
            {
                name: 'order.index',
                path: '/admin/orders',
                component: Order
            },
        ],
        meta: {
            requiresAuth: true,
        }
    },    
    // {
    //     path: '*',
    //     name: '404',
    //     component: NotFound,
    // },
];
