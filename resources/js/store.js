import { createStore } from 'vuex';
import axios from 'axios';

const store = createStore({
    state: {
        title: 'Home',
        isLoading: false,
        token: localStorage.getItem('access_token') || null,
        user: localStorage.getItem('user')
            ? JSON.parse(localStorage.getItem('user'))
            : {id: null, name: null, last_name: null, email: null}
    },
    getters: {
        loggedIn(state) {
            return state.token !== null;
        },
        token(state) {
            return state.token;
        },
        user(state) {
            return state.user;
        },
        isLoading(state) {
            return state.isLoading;
        },
        title(state) {
            return state.title;
        },
    },
    mutations: {
        retrieveToken(state, token) {
            state.token = token;
        },
        destroyToken(state) {
            state.token = null;
        },
        destroyUser(state) {
            state.user = {id: null, name: null, last_name: null, nickname: null, email: null, active: null, role_id: null};
        },
        retrieveUser(state, user) {
            state.user = user;
        },
        completed(state){
            state.isLoading = false;
        },
        loaded(state) {
            state.isLoading = true;
        },
        changeTitle(state, title) {
            state.title = title;
        },
    },
    actions: {
        retrieveToken(context, credentials) {
            return new Promise((resolve, reject) => {
              axios.post('/api/login', {
                email: credentials.email,
                password: credentials.password,
              }).then(response => {
                const token = response.data.data.tokens.auth.access_token;
                localStorage.setItem('access_token', token);
                localStorage.setItem('user', JSON.stringify(response.data.data.user));
                context.commit('retrieveToken', token);
                context.commit('retrieveUser', response.data.data.user);
                resolve(response);
              }).catch(error => {
                  reject(error);
              })
            })
          },
          destroyToken(context) {
            if (context.getters.loggedIn){
              return new Promise((resolve, reject) => {
                axios.post('/api/logout', '', {
                    headers: { Authorization: "Bearer " + context.state.token }
                })
                  .then(response => {
                    localStorage.removeItem('access_token');
                    localStorage.removeItem('user');
                    context.commit('destroyToken');
                    context.commit('destroyUser');
                    resolve(response);
                    })
                  .catch(error => {
                    localStorage.removeItem('access_token');
                    localStorage.removeItem('user');
                    context.commit('destroyToken');
                    context.commit('destroyUser');
                    reject(error);
                  });
              });
            } else {
              return new Promise((resolve, reject) => {
                  localStorage.removeItem('access_token');
                  localStorage.removeItem('user');
                  context.commit('destroyToken');
                  context.commit('destroyUser');
                  resolve('OK');
              });
      
            }
          },
    }
});

export default store;