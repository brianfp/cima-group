import { createRouter, createWebHistory } from 'vue-router';
import WebRoutes from './routes/web';
import AuthRoutes from './routes/auth';

const router = new createRouter({ history: createWebHistory()
    , routes: [
        ...WebRoutes,
        ...AuthRoutes,
    ]
});

export default router;